<?php

namespace phpUnitVoteApi\Test;

use MyApp\Core\VoteSeatsCalculator;

require_once 'App/Core/VoteSeatsCalculator.php';



class VoteSeatsCalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function providerCalculate()
    {
        return array (
                    array(
                        array(
                            'NumberOfSeats' => '15',
                            "voteResults" => array (
                                array("NameParty" => "A", "numberVotes" => 15000),
                                array("NameParty" => "B", "numberVotes" => 5400),
                                array("NameParty" => "C", "numberVotes" => 5500),
                                array("NameParty" => "D", "numberVotes" => 5550)
                            )
                        ),
                        array(
                                array("NameParty" => "D", "numberVotes" => 5550, "numberSeatsForThePartyInt" => 3),
                                array("NameParty" => "C", "numberVotes" => 5500, "numberSeatsForThePartyInt" => 3),
                                array("NameParty" => "B", "numberVotes" => 5400, "numberSeatsForThePartyInt" => 2),
                                array("NameParty" => "A", "numberVotes" => 15000, "numberSeatsForThePartyInt" => 7)
                        ),
                    ),
                    array(
                        array(
                            'NumberOfSeats' => '20',
                            "voteResults" => array (
                                array("NameParty" => "A", "numberVotes" => 35000),
                                array("NameParty" => "B", "numberVotes" => 5400),
                                array("NameParty" => "C", "numberVotes" => 5500),
                                array("NameParty" => "D", "numberVotes" => 5550)
                            )
                        ),
                        array(
                                array("NameParty" => "A", "numberVotes" => 35000, "numberSeatsForThePartyInt" => 14),
                                array("NameParty" => "D", "numberVotes" => 5550, "numberSeatsForThePartyInt" => 2),
                                array("NameParty" => "C", "numberVotes" => 5500, "numberSeatsForThePartyInt" => 2),
                                array("NameParty" => "B", "numberVotes" => 5400, "numberSeatsForThePartyInt" => 2)
                        ),
                    ),
                    array(
                        array(
                            'NumberOfSeats' => '20',
                            "voteResults" => array (
                                array("NameParty" => "A", "numberVotes" => 5000),
                                array("NameParty" => "B", "numberVotes" => 5000),
                                array("NameParty" => "C", "numberVotes" => 5000),
                                array("NameParty" => "D", "numberVotes" => 5000)
                            )
                        ),
                        array(
                                array("NameParty" => "A", "numberVotes" => 5000, "numberSeatsForThePartyInt" => 5),
                                array("NameParty" => "B", "numberVotes" => 5000, "numberSeatsForThePartyInt" => 5),
                                array("NameParty" => "C", "numberVotes" => 5000, "numberSeatsForThePartyInt" => 5),
                                array("NameParty" => "D", "numberVotes" => 5000, "numberSeatsForThePartyInt" => 5)
                        ),

                    ),
                    array(
                        array(
                            'NumberOfSeats' => '20',
                            "voteResults" => array (
                                array("NameParty" => "A", "numberVotes" => 10000),
                                array("NameParty" => "B", "numberVotes" => 3),
                                array("NameParty" => "C", "numberVotes" => 2),
                                array("NameParty" => "D", "numberVotes" => 1)
                            )
                        ),
                        array(
                            array("NameParty" => "A", "numberVotes" => 10000, "numberSeatsForThePartyInt" => 20),
                            array("NameParty" => "B", "numberVotes" => 3, "numberSeatsForThePartyInt" => 0),
                            array("NameParty" => "C", "numberVotes" => 2, "numberSeatsForThePartyInt" => 0),
                            array("NameParty" => "D", "numberVotes" => 1, "numberSeatsForThePartyInt" => 0)
                        ),

                    ),
                    array(
                        array(
                            'NumberOfSeats' => '20',
                            "voteResults" => array (
                                array("NameParty" => "A", "numberVotes" => 10000),
                            )
                        ),
                        array(
                            array("NameParty" => "A", "numberVotes" => 10000, "numberSeatsForThePartyInt" => 20),
                        ),

                    ),



        );
    }


    /**
     * @dataProvider providerCalculate
     * */
    public function testCalculate($receivedData, $expectedData)
    {
        $calculator = new VoteSeatsCalculator();
        $proceededVoteResults = $calculator->calculate($receivedData['NumberOfSeats'], $receivedData['voteResults']);
        $this->assertJsonStringEqualsJsonString(json_encode($expectedData), $proceededVoteResults);

    }

}
