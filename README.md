# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* VoteApi as a php REST API application for handle data with vote results
* Version 0.1

### What is algorithm for calculating seats ? ###
In the first step the corresponding number of votes that are attributable to a party is multiplied by the total number of available seats and divided by the total number of valid votes.
In the second step the result is split into the integer portion and the rest. The integer parts are attributed to the respective party as seats. The remaining seats are allocated to parties in the order of the size of the fractional portions.

###Exemplary data:
{"NumberOfSeats": 321, 
"voteResults": [{"NameParty":"A", "numberVotes":15000}, 
                {"NameParty":"B", "numberVotes":5400},
                {"NameParty":"C", "numberVotes":5500}, 
                {"NameParty":"D", "numberVotes":5550}]}
it means : total numbr of seats for all parties - 321
 party A has 15000 votes
 party B has 5400 votes
 ...


###Exemplary response data when you are retrieving record:

"11":{"data_vote":
    "{\"NumberOfSeats\": \"15\", 
    \"voteResults\": [{\"NameParty\":\"A\", 
                        \"numberVotes\":15000}, 
                        {\"NameParty\":\"B\",
                         \"numberVotes\":5400}, 
                         {\"NameParty\":\"C\", 
                         \"numberVotes\":5500}, 
                         {\"NameParty\":\"D\",
                          \"numberVotes\":5550}]}",
    "data_result":"[{\"NameParty\":\"D\",\"numberVotes\":555    0,\"numberSeatsForThePartyInt\":3},
                    {\"NameParty\":\"C\",\"numberVotes\":5500,\"numberSeatsForThePartyInt\":3},
                    {\"NameParty\":\"B\",\"numberVotes\":5400,\"numberSeatsForThePartyInt\":2},
                    {\"NameParty\":\"A\",\"numberVotes\":15000,\"numberSeatsForThePartyInt\":7}]","updated_at":{"date":"2017-08-27 00:00:00.000000","timezone_type":3,"timezone":"Europe\/Berlin"},"created_at":{"date":"2017-08-27 00:00:00.000000","timezone_type":3,"timezone":"Europe\/Berlin"}}}

data_vote - sended data to REST API before calculating seats
data_result - calculated data with assigned seats to each party

### How do I get set up? ###

* Installation

1) git clone https://bitbucket.org/dstiblo/voteapi/

2) cp files from voteapi directory to you webroot

3) create folder vendor

4) run command composer update 

* Configuration

1) open bootstrap.php and edit in accordance to your database settings

* How to run tests

1) cd to root folder of the project

2) php vendor/bin/phpunit


### Who do I talk to? ###
* If any question contact email denis.stiblo@gmail.com
