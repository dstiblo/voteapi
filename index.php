<?php
require 'vendor/autoload.php';
include 'bootstrap.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use MyApp\Models\Vote;
use MyApp\Core\VoteSeatsCalculator;
use Symfony\Component\Validator\Constraints as Assert;

date_default_timezone_set("Europe/Berlin");

$app = new Silex\Application();
$app->register(new Silex\Provider\ValidatorServiceProvider());

//accepting JSON
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->get('/votes/', function (Request $request) use ($app) {
    $votes = Vote::all();
    $payload = [];
    foreach ($votes as $vote){
        $payload[$vote->id] =
            [
                'data_vote' => $vote->data_vote,
                'data_result' => $vote->data_result,
                'updated_at' => $vote->updated_at,
                'created_at' => $vote->created_at,
            ];
    }
    return new Response(json_encode($payload, JSON_UNESCAPED_SLASHES), 200);
});

$app->get('/votes/{vote_id}', function (Silex\Application $app, $vote_id) use ($app) {
    $votes = Vote::where('id', $vote_id)->get();

    if ($votes->isEmpty()) {
        return new Response('Vote doesn\'t exist', 400);
    } else {
        $payload = [];
        foreach ($votes as $vote) {
            $payload[$vote->id] =
                [
                    'data_vote' => $vote->data_vote,
                    'data_result' => $vote->data_result,
                    'updated_at' => $vote->updated_at,
                    'created_at' => $vote->created_at,
                ];
        }
    }
    return new Response(json_encode($payload, JSON_UNESCAPED_SLASHES), 200);
});

$app->post('/votes/add', function (Request $request) use ($app) {


    $dataParsed = $request->request->all();

    $constraint = new Assert\Collection(array(
        'NumberOfSeats' => new Assert\Type('integer'),
        'voteResults' => new Assert\All(array(
            new Assert\Collection(array(
                'NameParty' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 1))),
                'numberVotes'  => new Assert\Type('integer'),
                ))

        )),
    ));
    $errors = $app['validator']->validate($dataParsed, $constraint);
    if (count($errors) > 0) {
        $errorMessage = '';
        foreach ($errors as $error) {
            $errorMessage .=  $error->getPropertyPath().' '.$error->getMessage()."\n";

        }

        return new Response($errorMessage, 400);

    } else {
        $seatsCalculator = new VoteSeatsCalculator();
        $vote = new Vote();
        $vote->data_vote = json_encode($dataParsed);
        $vote->data_result = $seatsCalculator->calculate($dataParsed['NumberOfSeats'], $dataParsed['voteResults']);
        $vote->save();
        return new Response('', 200);
    }

});


$app->delete('/vote/{vote_id}', function($vote_id) use ($app) {
    $vote = Vote::find($vote_id);
    if (is_object($vote)) {
        $vote->delete();
        if ($vote->exists) {
            return new Response('Vote doesn\'t exist', 400);
        } else {
            return new Response('', 200);
        }
    }
    else {
        return new Response('Vote doesn\'t exist', 400);
    }
});

$app->run();