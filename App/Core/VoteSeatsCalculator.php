<?php

namespace MyApp\Core;

class VoteSeatsCalculator
{
    public function calculate($totalSeatsNumber, $resultOfVotes)
    {
        if (sizeOf($resultOfVotes) == 0 ) {
            return '';
        }


        $totalVotesNumber  = array_sum(array_column($resultOfVotes, 'numberVotes'));

        $data['totalVotesNumber'] = $totalVotesNumber;
        $data['totalSeatsNumber'] = $totalSeatsNumber;
        array_walk($resultOfVotes, array($this, 'getSeatsByTotalVotes'), $data);

        $totalAssignedPartySeats  =  array_sum(array_column($resultOfVotes, 'numberSeatsForThePartyInt'));
        $numberNotAssignedSeats = $totalSeatsNumber - $totalAssignedPartySeats;
        if ($numberNotAssignedSeats > 0 ) {
            usort($resultOfVotes, function ($a, $b) {
                return ($a["numberSeatsForThePartyFraction"] < $b["numberSeatsForThePartyFraction"]);
            });


            for ($i = 0; $i < $numberNotAssignedSeats; $i++) {
                $resultOfVotes[$i]['numberSeatsForThePartyInt']++;
            }
        }

        array_walk($resultOfVotes, array($this, 'cleanUpResultOfVotes'));

        return json_encode($resultOfVotes);
    }


    private function getSeatsByTotalVotes(& $resultOfVoteItem, $key, $data)
    {
        $numberOfSeatsForParty = ($data['totalSeatsNumber'] * $resultOfVoteItem['numberVotes'])
            /$data['totalVotesNumber'];

        $resultOfVoteItem['numberSeatsForThePartyInt'] = intval(floor($numberOfSeatsForParty));
        $resultOfVoteItem['numberSeatsForThePartyFraction'] = $numberOfSeatsForParty - floor($numberOfSeatsForParty);



    }


    private function cleanUpResultOfVotes(& $resultOfVoteItem)
    {
        unset($resultOfVoteItem['numberSeatsForThePartyFraction']);
    }
}