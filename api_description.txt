Scenario: Test retrieving all votes for result
	Given the API at the URL "http://localhost/votes"
	When I send a GET request to the "votes/" path
	Then a 200 status code should be returned
	 And the request body should contain the JSON representation of all votes result in the database
	 example of returned data if database empty body should be empty ""
{"9":{"data_vote":"{\"NumberOfSeats\": \"15\", \"voteResults\": [{\"NameParty\":\"A\", \"numberVotes\":15000}},
"data_result":"[{\"NameParty\":\"D\",\"numberVotes\":5550,\"numberSeatsForThePartyInt\":3}]",
"updated_at":{"date":"2017-08-27 00:00:00.000000","timezone_type":3,"timezone":"Europe/Berlin"},
"created_at":{"date":"2017-08-27 00:00:00.000000","timezone_type":3,"timezone":"Europe/Berlin"}}

Scenario: Test retrieving  vote result by Vote id
	Given the API at the URL "http://localhost/votes/{vote_id}"
	When I send a GET request to the path "votes/{vote_id}""
	Then a 200 status code should be returned
	 And the request body should contain the JSON representation of vote with id from request
{"9":{"data_vote":"{\"NumberOfSeats\": \"15\", \"voteResults\": [{\"NameParty\":\"A\", \"numberVotes\":15000}},
"data_result":"[{\"NameParty\":\"D\",\"numberVotes\":5550,\"numberSeatsForThePartyInt\":3}]",
"updated_at":{"date":"2017-08-27 00:00:00.000000","timezone_type":3,"timezone":"Europe/Berlin"},
"created_at":{"date":"2017-08-27 00:00:00.000000","timezone_type":3,"timezone":"Europe/Berlin"}}


Scenario: Test deleting vote result by Vote id
	Given the API at the URL "http://localhost/votes/{vote_id}"
	When I send a DELETE request to the path "votes/{vote_id}""
	Then a 200 status code should be returned
	 And the requested vote result should be deleted from database

Scenario: Test inserting vote results
     Given the API at the URL "http://localhost/votes/add"
     When I send a POST request to the path "votes/add""
     Then a 200 status code should be returned
     And the new record with calculated seats for each party from request should be inserted in database
